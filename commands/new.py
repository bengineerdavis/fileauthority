import click
import ezodf


"""
A cli command to create various documents types
and formats

example: file new <name>.<ext> <dest>

"""

@click.command(
    short_help="Create a new file",
)
@click.argument('filename')
def new(filename, destination):
    """Takes creates a new file in the specified 
    directory. Must provide extension: <filename>.<ext>
    """
    opendoc = {'odt', 'ods',}
    name, ext = filename.split(".")
    if ext in opendoc:
        newfile = ezodf.newdoc()
        # TODO can I use this function to create a doc on any Path?
        newfile(ext, name)
        newfile.save()
    else:
        print(f"I don't know how to create {ext} types of files yet")
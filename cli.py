from pathlib import Path

import click
import ezodf
import magic

# from commands import new

@click.group()
def cli():
    pass

@cli.command(
    short_help="Create a new file",
)
@click.argument('filename')
def new(filename):
    """Takes creates a new file of the desired filetype in the specified 
    directory. Must provide extension: <filename>.<ext>. This will always
    default to the current directory unless an absolute path is given:
        <i/am/an/absolute/filepath>.<ext>
    """
    open_doctypes = {'odt', 'ods', 'odg', 'odp', 'odc', 'odi', 'odf', 'odm'}
    valid_doctypes = open_doctypes
    # ext must be included and validated in user provided in filename
    path = Path(filename).suffix
    ext = path.strip(".")
    if ext == 0:
        print(f"No filetype detected, please include a file extension format: '<filename>.<ext>' ")
    elif ext not in valid_doctypes:
        print(f"I don't know how to create {ext} types of files yet")
    else:
        newfile = ezodf.newdoc(doctype=ext, filename=filename)
        # TODO can I use this function to create a doc on any Path?
        newfile.save()        


# cli.add_command(new)
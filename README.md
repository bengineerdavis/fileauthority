# fileauthority

## What is does
Automatically find, rename, and move recently downloaded files to your current directory. It will also detect the file type and, if file is text, remove excess whitespace (more than one ' ' between each word or sentence).

Use as follows:
`fileauthority '<what we want to rename file>'`

For a sample text file to demonstrate the command above:
https://filesamples.com/formats/txt

## Why would I need or want this?
To avoid a common, manual process that is
- Repetitive
- Easy to mess up
- Distracting
- Boring

## How it does it
After you run the script, it scans default folders for available files, and turns all those files into a top-down interactive list (fzf) that will match file names based on what text you type in, and let you choose which file you want to move and rename. You have to do this from the terminal and run the command from inside the directory where you want the renamed, moved file to live in. But now, from one place with a one-line command, you get an ungarbled, properly named file in the directory of your choice.

## Installation

### In terminal (on your computer, using bash)

#### Before you begin...

- [ ] You must have a recent version of Python 3
    - run ```python -V``` in the terminal

- [ ] You must complete the OS-specific [installation instructions](https://github.com/ahupp/python-magic#installation) for libmagic library.

#### Create new directory
- [ ] `mkdir lets-try-fileauthority`
- [ ] `cd lets-try-fileauthority`

#### Clone repository
- (ssh) `git clone git@gitlab.com:bengineerdavis/fileauthority.git`
- (https) `git clone https://gitlab.com/bengineerdavis/fileauthority.git`

#### Create virtual environment
- [ ] `python3 -m venv venv`

#### Activate virutal
- [ ] `source venv/bin/activate`

**Note:** Always use an activated virtual environment to run this script

#### Install requirements
 
- [ ] `python -m pip install --upgrade pip`
- [ ] `pip install -r requirements.txt`
